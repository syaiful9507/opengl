

#include <windows.h>
#include <math.h>
#include <GL/glut.h>
#include <stdlib.h>

typedef struct{
    float x; float y;
}point2D_t;
typedef struct{
    int x; int y;
}point2D_i;
typedef struct{
    float x,y,z;
}point3D_t;
typedef struct{
    float v[3];
}vector3D_t;
typedef struct{
    float m[3][3];
}matrix3D_t;
typedef struct{
    int m[3][3];
}matrix3D_i;
typedef struct{
    int NumberofVertices;
    short int pnt[32];
}face_t;
typedef struct{
    int NumberofVertices;
    point3D_t pnt[100];
    int NumberofFace;
    face_t fce[32];
}object3D_t;
typedef struct {
	float r, g, b;
} Color_t;
typedef struct{
    float x,y,z,r,g,b;
}object3D_color_t;

float sudut;
matrix3D_t createIdentity(){
    matrix3D_t rotate;
    rotate.m[0][0]=0.0;
    rotate.m[0][1]=0.0;
    rotate.m[0][2]=0.0;

    rotate.m[1][0]=0.0;
    rotate.m[1][1]=0.0;
    rotate.m[1][2]=0.0;

    rotate.m[2][0]=0.0;
    rotate.m[2][1]=0.0;
    rotate.m[2][2]=0.0;
    return rotate;
}
matrix3D_t rotationX(float teta){
    matrix3D_t rotate=createIdentity();
    rotate.m[0][0]=1.0;
    rotate.m[0][1]=0.0;
    rotate.m[0][2]=0.0;

    rotate.m[1][0]=0.0;
    rotate.m[1][1]=cos(teta/57.3);
    rotate.m[1][2]=-sin(teta/57.3);

    rotate.m[2][0]=0.0;
    rotate.m[2][1]=sin(teta/57.3);
    rotate.m[2][2]=cos(teta/57.3);
    return rotate;
}
matrix3D_t rotationY(float teta){
    matrix3D_t rotate=createIdentity();
    rotate.m[0][0]=cos(teta/57.3);
    rotate.m[0][1]=0.0;
    rotate.m[0][2]=sin(teta/57.3);

    rotate.m[1][0]=0.0;
    rotate.m[1][1]=1.0;
    rotate.m[1][2]=0.0;

    rotate.m[2][0]=-sin(teta/57.3);
    rotate.m[2][1]=0.0;
    rotate.m[2][2]=cos(teta/57.3);
    return rotate;
}
matrix3D_t rotationZ(float teta){
    matrix3D_t rotate=createIdentity();
    rotate.m[0][0]=cos(teta/57.3);
    rotate.m[0][1]=-sin(teta/57.3);
    rotate.m[0][2]=0.0;

    rotate.m[1][0]=sin(teta/57.3);
    rotate.m[1][1]=cos(teta/57.3);
    rotate.m[1][2]=0.0;

    rotate.m[2][0]=0.0;
    rotate.m[2][1]=0.0;
    rotate.m[2][2]=1.0;
    return rotate;
}
vector3D_t operator +(vector3D_t a, vector3D_t b){
    vector3D_t c;
    for(int i=0;i<3;i++){
        c.v[i]=a.v[i]+b.v[i];
    }return c;
}
vector3D_t operator -(vector3D_t a, vector3D_t b){
    vector3D_t c;
    for(int i=0;i<3;i++){
        c.v[i]=a.v[i]+b.v[i];
    }return c;
}
vector3D_t operator *(matrix3D_t a, vector3D_t b){
    vector3D_t c;
    for(int i=0;i<3;i++){
        c.v[i]=0;
        for (int j=0;j<3;j++){
            c.v[i]+=a.m[i][j]*b.v[j];
        }
    }return c;
}
void setColor(Color_t col){
	glColor3f(col.r, col.g, col.b);
}
void create3DObject(object3D_t object, Color_t col){
        for(int i=0; i<object.NumberofFace; i++){
        setColor(col);
        glBegin(GL_POLYGON);
        for(int j=0; j<object.fce[i].NumberofVertices; j++){
            int p=object.fce[i].pnt[j];
            float x=object.pnt[p].x;
            float y=object.pnt[p].y;
           glVertex3f(x, y, 0.0);
           }
       glEnd();
       }
}
void create3DObjectG(object3D_t object, Color_t col){
        for(int i=0; i<object.NumberofFace; i++){
        setColor(col);
        glBegin(GL_LINE_STRIP);
        for(int j=0; j<object.fce[i].NumberofVertices; j++){
            int p=object.fce[i].pnt[j];
            float x=object.pnt[p].x;
            float y=object.pnt[p].y;
           glVertex3f(x, y, 0.0);
           }
       glEnd();
       }
}
 object3D_t prisma1={12,
        {{-50,100,0},{0,100,0},{0,0,0},
            {70,0,0},{70,-50,0},{-50,-50,0}, 
			
			{-50,100,-50},{0,100,-50},{0,0,-50},
            {70,0,-50},{70,-50,-50},{-50,-50,-50}
			},
            
        10,{{4,{0,1,2,5}},{4,{2,3,4,5}},
            {4,{6,7,8,11}},{4,{8,9,10,11}},
            {4,{0,1,7,6}},{4,{2,8,9,3}},
			
			{4,{3,9,10,4}},{4,{4,5,11,10}},{4,{0,6,11,5}},{4,{1,7,8,2}},
}};

//matrix3D_t matrix_X=rtationX(sudut);
void userdraw() {
    matrix3D_t matrix_X=rotationX(sudut);
     matrix3D_t matrix_Y=rotationY(sudut);
     matrix3D_t matrix_Z=rotationZ(sudut);
     for(int i=0; i<prisma1.NumberofVertices; i++){
        //teta=15.0;
        vector3D_t p;
        p.v[0]=prisma1.pnt[i].x;
        p.v[1]=prisma1.pnt[i].y;
        p.v[2]=prisma1.pnt[i].z;
        p=(matrix_Y)*(p);   //p=operator *(matrix_Y,p);
        p=(matrix_X)*(p);   //p=operator *(matrix_X,p);
        p=(matrix_Z)*(p);   //p=operator *(matrix_Z,p);
        prisma1.pnt[i].x=p.v[0];
        prisma1.pnt[i].y=p.v[1];
        prisma1.pnt[i].z=p.v[2];
        }

Color_t color1;
color1.r=0.0;
color1.g=0.0;
color1.b=1.0;
    create3DObject(prisma1,color1);
Color_t color2;
color1.r=0.0;
color1.g=1.0;
color1.b=1.0;
    create3DObjectG(prisma1,color2);
    sudut=2; if(sudut>=360.0) sudut=0.0;
glFlush();
}
void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    userdraw();
    glutSwapBuffers();
}
void timer(int value){
	glutPostRedisplay();
	glutTimerFunc(30, timer, 0);
}
int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB );
	glutInitWindowPosition(100,100);
	glutInitWindowSize(640,480);
	glutCreateWindow ("SYAIFUL");
	glClearColor(1.0, 1.5, 0.0, 1.5);
	gluOrtho2D(-320., 320., -240.0, 240.0);
	glutDisplayFunc(display);
	glutTimerFunc(1,timer,0);
	glutMainLoop();
	return 0;
}

