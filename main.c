#include <stdlib.h>
#include <cmath>
#include <math.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <conio.h>
using namespace std;
ofstream myfile;
bool tulis=true;


typedef struct{
    float x; float y;
}point2D_t;
typedef struct{
    int x; int y;
}point2D_i;
typedef struct{
    float x,y,z,r,g,b;
}point3D_t;
typedef struct{
    float v[3];
}vector3D_t;
typedef struct{
    float m[3][3];
}matrix3D_t;
typedef struct{
    int m[3][3];
}matrix3D_i;
typedef struct{
    int NumberofVertices;
    short int pnt[32];
}face_t;
typedef struct{
    int NumberofVertices;
    point3D_t pnt[100];
    int NumberofFace;
    face_t fce[32];
}object3D_t;
typedef struct {
	float r, g, b;
} Color_t;
float sudut;
float sudutlagi;
matrix3D_t createIdentity(){
    matrix3D_t rotate;
    rotate.m[0][0]=0.0;
    rotate.m[0][1]=0.0;
    rotate.m[0][2]=0.0;

    rotate.m[1][0]=0.0;
    rotate.m[1][1]=0.0;
    rotate.m[1][2]=0.0;

    rotate.m[2][0]=0.0;
    rotate.m[2][1]=0.0;
    rotate.m[2][2]=0.0;
    return rotate;
}
matrix3D_t rotationX(float teta){
    matrix3D_t rotate=createIdentity();
    rotate.m[0][0]=1.0;
    rotate.m[0][1]=0.0;
    rotate.m[0][2]=0.0;

    rotate.m[1][0]=0.0;
    rotate.m[1][1]=cos(teta/57.3);
    rotate.m[1][2]=-sin(teta/57.3);

    rotate.m[2][0]=0.0;
    rotate.m[2][1]=sin(teta/57.3);
    rotate.m[2][2]=cos(teta/57.3);
    return rotate;
}
matrix3D_t rotationY(float teta){
    matrix3D_t rotate=createIdentity();
    rotate.m[0][0]=cos(teta/57.3);
    rotate.m[0][1]=0.0;
    rotate.m[0][2]=sin(teta/57.3);

    rotate.m[1][0]=0.0;
    rotate.m[1][1]=1.0;
    rotate.m[1][2]=0.0;

    rotate.m[2][0]=-sin(teta/57.3);
    rotate.m[2][1]=0.0;
    rotate.m[2][2]=cos(teta/57.3);
    return rotate;
}
matrix3D_t rotationZ(float teta){
    matrix3D_t rotate=createIdentity();
    rotate.m[0][0]=cos(teta/57.3);
    rotate.m[0][1]=-sin(teta/57.3);
    rotate.m[0][2]=0.0;

    rotate.m[1][0]=sin(teta/57.3);
    rotate.m[1][1]=cos(teta/57.3);
    rotate.m[1][2]=0.0;

    rotate.m[2][0]=0.0;
    rotate.m[2][1]=0.0;
    rotate.m[2][2]=1.0;
    return rotate;
}
vector3D_t operator +(vector3D_t a, vector3D_t b){
    vector3D_t c;
    for(int i=0;i<3;i++){
        c.v[i]=a.v[i]+b.v[i];
    }return c;
}
vector3D_t operator -(vector3D_t a, vector3D_t b){
    vector3D_t c;
    for(int i=0;i<3;i++){
        c.v[i]=a.v[i]+b.v[i];
    }return c;
}
vector3D_t operator *(matrix3D_t a, vector3D_t b){
    vector3D_t c;
    for(int i=0;i<3;i++){
        c.v[i]=0;
        for (int j=0;j<3;j++){
            c.v[i]+=a.m[i][j]*b.v[j];
        }
    }return c;
}
vector3D_t operator ^ (vector3D_t a, vector3D_t b){
    vector3D_t c;
    c.v[0]=a.v[1]*b.v[2]-a.v[2]*b.v[1];
    c.v[1]=a.v[2]*b.v[0]-a.v[0]*b.v[2];
    c.v[2]=a.v[0]*b.v[1]-a.v[1]*b.v[0];
    c.v[3]=1.;
    return c;
}
void setColor(Color_t col){
	glColor3f(col.r, col.g, col.b);
}
vector3D_t poin2vector(point3D_t pnt){
        vector3D_t vec;
        vec.v[0]=pnt.x;
        vec.v[1]=pnt.y;
        vec.v[2]=pnt.z;
}
void create3DObject(object3D_t object){
    vector3D_t normalVector;
    vector3D_t vecbuff[3];
    int normal;
    short int a0;
    float ab = 0.4;
    for(int k=0;k<object.NumberofFace;k++){
        for(int u;u<object.fce[k].NumberofVertices;u++){
            a0=object.fce[u].pnt[k];
            vecbuff[u]=poin2vector(object.pnt[a0]);
        }
            normalVector=(vecbuff[1]-vecbuff[0])^(vecbuff[2]-vecbuff[0]);
            normal=normalVector.v[2];
            if (normal>=0){
                for(int i=0; i<object.NumberofFace; i++){
                glColor3f(1.0, 0.0, 0.0);
                glBegin(GL_POLYGON);
                for(int j=0; j<object.fce[i].NumberofVertices; j++){
                    int p=object.fce[i].pnt[j];
                    float x=object.pnt[p].x*ab;
                    float y=object.pnt[p].y*ab;
                   glVertex3f(x, y, 0.0);
                   }
               glEnd();
               }
            }else{
                for(int i=0; i<object.NumberofFace; i++){
                glColor3f(1,1,1);
                glBegin(GL_POLYGON);
                for(int j=0; j<object.fce[i].NumberofVertices; j++){
                    int p=object.fce[i].pnt[j];
                    float x=object.pnt[p].x*ab;
                    float y=object.pnt[p].y*ab;
                   glVertex3f(x, y, 0.0);
                   }
               glEnd();
               }
            }
        }
if(tulis){
    myfile.open("D:/MY_DATA/POSTEST/UP/UP4/BentukKeempat.off",ios::app);
    myfile.close();
    myfile.open("D:/MY_DATA/POSTEST/UP/UP4/BentukKeempat.off",ios::trunc);
    cout<<"Operasi File"<<endl;
    cout<<"-------------"<<endl;
    if(!myfile.fail()){
            myfile<<"COFF SYAIFUL"<<endl;
             myfile<<object.NumberofVertices<<" "<<object.NumberofFace<<" "<< 0<<endl;
                for(int h=0;h<object.NumberofVertices;h++){

                    myfile<<std::setprecision(3)<<object.pnt[h].x*ab<<" "<<object.pnt[h].y*ab<<" "<<object.pnt[h].z*ab<<" "<<object.pnt[h].r<<" "<<object.pnt[h].g<<" "<<object.pnt[h].b<<" "<<255<<endl;

                }


            for (int m=0;m<object.NumberofFace;m++){

                    myfile<<object.fce[m].NumberofVertices<<" "<<object.fce[m].pnt[0]<<" "<<object.fce[m].pnt[1]<<" "<<object.fce[m].pnt[2]<<" "<<object.fce[m].pnt[3]<<endl;

            }
        myfile.close();
        std::ifstream  ifs1("D:/MY_DATA/POSTEST/UP/UP4/BentukKeempat.off");
        if(!ifs1){
       std::cout << "cannot open file_readoff";
       exit(1);
        }else{
            char c=ifs1.get();
            while (ifs1.good()){
                std::cout << c;
                c=ifs1.get();
            }
        }

    }else{
        cout<<"file tidak ditemukan"<<endl;
    }
}

if(sudutlagi>90){
tulis=false;

}else{
sudutlagi += sudut;
}

} 

object3D_t huruf={12,
        			{{-50,100,0},
					{0,100,0},
					{0,0,0},
            		{70,0,0},
					{70,-50,0},
					{-50,-50,0}, 
					{-50,100,-50},
					{0,100,-50},
					{0,0,-50},
            		{70,0,-50},
					{70,-50,-50},
					{-50,-50,-50}
				},
            
        	10,{
					{4,{0,1,2,5}},
					{4,{2,3,4,5}},
           			{4,{6,7,8,11}},
					{4,{8,9,10,11}},
            		{4,{0,1,7,6}},
					{4,{2,8,9,3}},
					{4,{3,9,10,4}},
					{4,{4,5,11,10}},
					{4,{0,6,11,5}},
					{4,{1,7,8,2}},
				}
	};

void userdraw() {
    matrix3D_t matrix_X=rotationX(sudut);
     matrix3D_t matrix_Y=rotationY(sudut);
     matrix3D_t matrix_Z=rotationZ(sudut);
     for(int i=0; i<huruf.NumberofVertices; i++){
        //teta=15.0;
        vector3D_t p;
        p.v[0]=huruf.pnt[i].x;
        p.v[1]=huruf.pnt[i].y;
        p.v[2]=huruf.pnt[i].z;
        p=(matrix_Y)*(p); 
        p=(matrix_X)*(p);   
        p=(matrix_Z)*(p);   
        huruf.pnt[i].x=p.v[0];
        huruf.pnt[i].y=p.v[1];
        huruf.pnt[i].z=p.v[2];
        }
    create3DObject(huruf);
    sudut=2; if(sudut>=360.0) sudut=0.0;


glFlush();
}
void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    userdraw();
    glutSwapBuffers();
}
void timer(int value){
	glutPostRedisplay();
	glutTimerFunc(30, timer, 0);
}
int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB );
	glutInitWindowPosition(100,100);
	glutInitWindowSize(640,480);
	glutCreateWindow ("SYAIFUL");
	glClearColor(1.0, 1.0, 1.0, 1.0);
	gluOrtho2D(-320., 320., -240.0, 240.0);
	glutDisplayFunc(display);
	glutTimerFunc(1,timer,0);
	glutMainLoop();
	return 0;
}